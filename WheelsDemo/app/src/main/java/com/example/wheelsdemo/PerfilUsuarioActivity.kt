package com.example.wheelsdemo

import android.content.ContentValues.TAG
import android.util.Base64
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.toBitmap
import com.example.wheelsdemo.MainActivity.Companion.auth
import com.example.wheelsdemo.databinding.ActivityPerfilUsuarioBinding
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.firestoreSettings
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.*
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*


class PerfilUsuarioActivity : AppCompatActivity() {

    lateinit var imageView2:ImageView
    lateinit var sharedPref: SharedPreferences
    lateinit var binding:ActivityPerfilUsuarioBinding
    lateinit var document: DocumentSnapshot

    var user: FirebaseUser? = Firebase.auth.currentUser
    private val db = Firebase.firestore
    val settings = firestoreSettings {
        isPersistenceEnabled = true
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPerfilUsuarioBinding.inflate(layoutInflater)
        setContentView(binding.root)

        sharedPref = getSharedPreferences("sharedPref", MODE_PRIVATE)
        imageView2 = binding.perfilImageView
        db.firestoreSettings = settings
        getFirebaseData()

        imageView2 = binding.perfilImageView


        // Al usar commit exigo que la escritura se haga de forma sincrona,
        // por lo tanto debo ejecutar esta tarea en un thread secundario y no el principal
        // este metodo busca la imagen en galeria
        binding.cargarImagenButton.setOnClickListener {
            if(isConnected()){
                findImage()
            } else {
                showAlert("Ups parece que tienes un fallo en tu conexion, intenta más tarde", "Problema de conexion")
            }
        }

        binding.cargarPerfilButton.setOnClickListener {
            loadProfile(user)
            ocultarBotones(it)
        }

        binding.editarPerfilButton.setOnClickListener {
            enableEdition()
        }
        binding.cancelarButton.setOnClickListener {
            ocultarBotones(it)
        }
    }

    // para revisar la conexion
    fun isConnected():Boolean{
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        return isConnected
    }

    // funciones de los botones

    fun ocultarBotones(view: View){
        binding.nombreTextView.visibility = View.VISIBLE
        binding.telefonoTextView.visibility = View.VISIBLE
        binding.editTextTextPersonName.visibility = View.GONE
        binding.editTextTextPersonName2.visibility = View.GONE
        binding.editarPerfilButton.visibility = View.VISIBLE
        binding.cancelarButton.visibility = View.GONE
        binding.cargarPerfilButton.visibility = View.GONE
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun enableEdition(){
        if(isConnected()){
            binding.nombreTextView.visibility = View.GONE
            binding.telefonoTextView.visibility = View.GONE
            binding.editTextTextPersonName.visibility = View.VISIBLE
            binding.editTextTextPersonName2.visibility = View.VISIBLE
            binding.editarPerfilButton.visibility = View.GONE
            binding.cancelarButton.visibility = View.VISIBLE
            binding.cargarPerfilButton.visibility = View.VISIBLE
        } else {
            showAlert("Ups parece que tienes un fallo en tu conexion, intenta más tarde", "Problema de conexion")
        }

    }

    // Metodos para manejar la foto de perfil

    fun findImage(){
        var galeriaIntent = Intent(Intent.ACTION_PICK)
        galeriaIntent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        resultLauncher.launch(galeriaIntent)
    }

    suspend fun guardarImagenPreferences(){
        val baos = ByteArrayOutputStream()
        val bitmap = imageView2.drawable.toBitmap()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)
        val encodedImage = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT)
        val user = hashMapOf(
            "encodedImage" to encodedImage
        )

        // Add a new document with a generated ID
        updateUser(this.user,user)
        withContext(Dispatchers.IO){
            with(sharedPref.edit()) {
                putString("encodedImage", encodedImage)
                apply()
            }

        }
    }

    fun loadImagen(imageView: ImageView, context: Context) {
        /**
         * Loads the last saved image from SharedPreferences into the
         * MainActivity's ImageView
         */
        var encodedImage:String? = "DEFAULT"
        encodedImage = sharedPref.getString("encodedImage", "DEFAULT")

        if (encodedImage != "DEFAULT") {
            val imageBytes = Base64.decode(encodedImage, Base64.DEFAULT)
            val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
            imageView.setImageBitmap(decodedImage)
        }
    }

    fun loadTexto(){
        var nombre:String? = "Nombre"
        var phone:String? = "Numero"
        nombre = sharedPref.getString("name", "Nombre")
        phone = sharedPref.getString("number", "Numero")

        if (nombre != "Nombre") {
            binding.nombreTextView.text = sharedPref.getString("name", "Nombre")
        }
        if (phone != "Numero") {
            binding.telefonoTextView.text = sharedPref.getString("number", "Numero")
        }
    }

    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if(result.resultCode == RESULT_OK && result.data != null){
            var selectedImage = result.data?.data
            //useImage(selectedImage)
            imageView2.setImageURI(selectedImage)
            try {
                if(!isConnected()){
                    showAlert("Tus cambios seran efectivos cuando estes conectado a internet", "Problema de conexion")
                }
                val externalScope: CoroutineScope = GlobalScope
                externalScope.launch {
                    guardarImagenPreferences()
                }

                showAlert("Espera un momento para ver tu imagen cargada")
            } catch (e: IOException){
                showAlert("Tu imagen no se ha cargado, porfavor intenta de nuevo")
            }
        } else {
            showAlert("Tu imagen no se ha cargado, porfavor intenta de nuevo")
        }
    }

    // Metodos para manejar la informacion de perfil

    private fun loadProfile(authUser: FirebaseUser?){

        if(!isConnected()){
            showAlert("Tus cambios seran efectivos cuando estes conectado a internet", "Problema de conexion")
        }
        // Create a new user with a first and last name
        var calendar = Calendar.getInstance()
        var day = calendar.get(Calendar.DAY_OF_WEEK)

        var name: String? = binding.editTextTextPersonName.text.toString()
        var phone: String? = binding.editTextTextPersonName2.text.toString()

        val user = hashMapOf(
            "name" to name,
            "number" to phone,
            "day" to day,
            "avg" to "1300"
        ) as Map<String, Any>

        with(sharedPref.edit()) {
            putString("name", name)
            putString("number", phone)
            apply()
        }

        // Add a new document with a generated ID
        updateUser(authUser, user)
    }

    fun actualizarSharedPref(){
        var name = document.getString("name")
        var phone = document.getString("number")
        with(sharedPref.edit()) {
            putString("name", name)
            putString("number", phone)
            apply()
        }
        binding.nombreTextView.text = sharedPref.getString("name", "Nombre")
        binding.telefonoTextView.text = sharedPref.getString("number", "Numero")
    }

    fun updateUser(authUser: FirebaseUser?, user:Map<String, Any>){
        db.collection("users").document(authUser?.email.toString())
            .update(user)
            .addOnSuccessListener { eo ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${authUser?.email}")
                getFirebaseData()
                showAlert("Tus datos han sido actualizados!!!!", "Datos actualizados")
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }
    }

    private fun showAlert(message:String?, title:String?="Estado de carga"){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
        builder.setMessage(message)
        val dialog = builder.create()
        dialog.show()
    }

    fun getFirebaseData(){
        // Asignar los textfield con estos datos
        val docRef = db.collection("users").document(user?.email.toString())
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    this.document = document
                    Log.d(TAG, "DocumentSnapshot data: ${document.data}")
                    // traer datos de firebase, document.getString("name")

                    actualizarSharedPref()
                    if(!sharedPref.contains("encodedImage") && document.getString("encodedImage")!=null){
                        val imageBytes = Base64.decode(document.getString("encodedImage"), Base64.DEFAULT)
                        val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
                        imageView2.setImageBitmap(decodedImage)
                        val externalScope: CoroutineScope = GlobalScope
                        externalScope.launch {
                            guardarImagenPreferences()
                        }
                    }
                    //Log.d(TAG, "DocumentSnapshot data: ${binding.textView3.text}")

                } else {
                    Log.d(TAG, "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d(TAG, "get failed with ", exception)
            }
    }


    // Metodos para el control del ciclo de vida de esta actividad

    override fun onStart() {
        super.onStart()
        sharedPref = getSharedPreferences("sharedPref", Context.MODE_PRIVATE)
        loadImagen(imageView2,applicationContext)
        loadTexto()
        //getFirebaseData()
        //Toast.makeText(applicationContext, "onStart called", Toast.LENGTH_LONG).show()
    }

}