package com.example.wheelsdemo
import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.collection.ArrayMap
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.Request
import java.lang.Exception
import com.example.wheelsdemo.DTO.MapDTO
import kotlinx.coroutines.launch
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener{

    private lateinit var mMap: GoogleMap
    var myCoordiates = LatLng(0.0,0.0)
    var otherLocation = LatLng(0.0,0.0)
    var data = ""
    var url = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_map_route_traffic)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onMapReady(googleMap: GoogleMap) {

        if(isOnline(this)){
            mMap = googleMap
            val coordinates=intent.getStringExtra("coordinates")
            val coor = coordinates?.split(",")
            myCoordiates  = getLocation()
            otherLocation = LatLng(coor!![0].toDouble(), coor!![1].toDouble())
            coor?.get(0)?.let { createMarket(it?.let { coor[1]?.let { it1 -> LatLng(it.toDouble(), it1.toDouble()) } }) }
            url = getURL(LatLng(myCoordiates.latitude, myCoordiates.longitude), otherLocation)

            createRoute(url)
        }
        else{
            createNotification("No hay conexión a internet", "para cargar la información del viaje es necesario tener conexión a internet","", "Internet")
        }

        var key = "${otherLocation.latitude}${otherLocation.longitude}+${myCoordiates.latitude}+${myCoordiates.longitude}"
        var arraymap = saveRouteCache(key,data)
        storeVariables()
        if(isOnline(this)){
            createRoute(url)
        }
        else{
            arraymap[key]?.let { createRoute(it) }
            createNotification("No hay conexión a internet", "La ruta mostrada no está actualizada con la información del conductor en vivo","", "Internet")
        }
        mMap.setOnMyLocationButtonClickListener(this)
        enableLocation()
        mMap.uiSettings.setAllGesturesEnabled(true)
        with(mMap.uiSettings) {
            mMap.isTrafficEnabled = false
            isZoomControlsEnabled = true
            isCompassEnabled = true
            isMyLocationButtonEnabled = true
            isMapToolbarEnabled = true
            isZoomGesturesEnabled = true
            isScrollGesturesEnabled = true
            isTiltGesturesEnabled = true
            isRotateGesturesEnabled = true
        }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("MissingPermission")
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }
    fun saveRouteCache(key: String, data: String): ArrayMap<String, String> {
        var tripData = ArrayMap<String, String>()
        tripData[key] = data
        return tripData
    }

    /*
Function that stores the variables in the Shared Preferences of the activity.
 */
    fun storeVariables() {
        val sharedPref = getPreferences(Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putString("USER_COORDINATES", myCoordiates.toString())
            putString("OTHER_COORDINATES", otherLocation.toString())
            apply()
        }
    }

    @SuppressLint("MissingPermission")
    fun getLocation(): LatLng {
        var ubicacion = LatLng(0.0,0.0)
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSION_REQUEST_ACCESS_FINE_LOCATION)
        }
        val loc2 = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        val listPermissionsNeeded: List<String> = ArrayList()
        try{
            var locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
            val lastKnownLocationByGps =
                locationManager?.getLastKnownLocation(LocationManager.GPS_PROVIDER)

//------------------------------------------------------//
            val lastKnownLocationByNetwork =
                locationManager?.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
//------------------------------------------------------//
            var latitude = lastKnownLocationByGps?.latitude
            var longitude = lastKnownLocationByGps?.longitude
            var ubicacionLat = latitude?.let { it }!!
            var ubicacionLong = longitude?.let { it }!!
            ubicacion = LatLng(ubicacionLat,ubicacionLong)
        }catch (e:Exception){

        }

        return ubicacion
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST_ACCESS_FINE_LOCATION) {
            when (grantResults[0]) {
                PackageManager.PERMISSION_GRANTED -> getLocation()
                PackageManager.PERMISSION_DENIED -> createNotification("No se tienen los permisos necesarios", "Es necesario que aceptes los permisos de ubicación para poder dar una ruta", "xd", "permisos" ) //Tell to user the need of grant permission
            }
        }
    }

    companion object {
        const val PERMISSION_REQUEST_ACCESS_FINE_LOCATION = 100
        const val REQUEST_CODE_LOCATION=0
    }

    private fun createRoute(url : String) {
        Thread(Runnable {
            val client = OkHttpClient()
            Log.i("La url es", "salud2")

            val request = Request.Builder().url(url).build()
            Log.i("La url es", "salud3")

            val response = client.newCall(request).execute()
            Log.i("La url es", "salud4")

            val data = response.body()!!.string()
            print(data)
            runOnUiThread {
                val result = ArrayList<List<LatLng>>()
                try {
                    val respObj = Gson().fromJson(data,MapDTO::class.java)
                    val path = ArrayList<LatLng>()

                    for (i in respObj.routes[0].legs[0].steps) {
                        path.addAll(decodePoly(i.polyline.points))
                    }
                    result.add(path)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                val lineoption = PolylineOptions()
                for (i in result.indices) {
                    lineoption.addAll(result[i])
                    lineoption.width(10f)
                    lineoption.color(Color.RED)
                    lineoption.geodesic(true)
                }
                mMap.addPolyline(lineoption)
            }
        }).start()
    }



    private fun getURL(from : LatLng, to : LatLng) : String {
        val origin = "origin=" + from.latitude + "," + from.longitude
        val dest = "destination=" + to.latitude + "," + to.longitude
        val sensor = "sensor=false"
        val key = "key=AIzaSyASQk-X7xDOWu876b4B1LbIs_w5hXFugFM"
        val params = "$origin&$dest&$sensor&$key"
        Log.i("La url es", "https://maps.googleapis.com/maps/api/directions/json?$params\"")

        return "https://maps.googleapis.com/maps/api/directions/json?$params"
    }

    /** Returns whether the checkbox with the given id is checked */
    private fun createMarket( ubiDestino: LatLng ){
        val marketDestination =MarkerOptions().position(ubiDestino).title("Tu destino se encuentra aquí")

        mMap.addMarker(marketDestination)
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ubiDestino, 18f),4000,null)
    }
    private fun isLocationPermissionGranted()= ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED
    @SuppressLint("MissingPermission")
    private fun enableLocation(){
        if(!::mMap.isInitialized)return
        if(isLocationPermissionGranted()){
            mMap.isMyLocationEnabled = true
        }
        else{
            requestLocationPermission()
        }
    }
    private fun requestLocationPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(this,"Ve a los ajustes y acepta los permisos", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                Companion.REQUEST_CODE_LOCATION
            )
        }
    }



    @SuppressLint("MissingPermission")
    override fun onResumeFragments() {
        super.onResumeFragments()
        if(!::mMap.isInitialized)return
        if(!isLocationPermissionGranted()){
            mMap.isMyLocationEnabled = false
            Toast.makeText(this,"Para activar laa localización, ve a ajustes y acepta los permisos", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onMyLocationButtonClick(): Boolean {
        Toast.makeText(this,"Boton Pulsado", Toast.LENGTH_SHORT).show()
        return false
    }

    fun createNotification(title: String, text: String, bigText: String, CHANNEL_ID:String){
        var builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(title)
            .setContentText(text)
            .setStyle(NotificationCompat.BigTextStyle()
                .bigText(bigText))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        with(NotificationManagerCompat.from(this)) {
            // notificationId is a unique int for each notification that you must define
            notify(1, builder.build())
        }
    }

    /**
     * Method to decode polyline points
     * Courtesy : https://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     */
    private fun decodePoly(encoded: String): List<LatLng> {
        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val p = LatLng(lat.toDouble() / 1E5,
                lng.toDouble() / 1E5)
            poly.add(p)
        }

        return poly
    }
}