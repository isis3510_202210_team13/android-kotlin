package com.example.wheelsdemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.wheelsdemo.databinding.ActivityErrorBinding
import com.example.wheelsdemo.databinding.ActivityMainBinding

class ErrorActivity : AppCompatActivity() {

    lateinit var binding: ActivityErrorBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityErrorBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.erroButton.setOnClickListener {
            var mainIntent = Intent(this, MainActivity::class.java)
            if(mainIntent.resolveActivity(packageManager)!=null){
                startActivity(mainIntent)
            }
        }


    }
}